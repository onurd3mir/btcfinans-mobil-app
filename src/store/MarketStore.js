import {observable,action,runInAction} from 'mobx';
import axios from 'axios';
import {API_BASE} from '../constant';
import AuthStore from './AuthStore';

class MarketStore {

    @observable markets = [];
    @observable loading = true;

    @observable lastOrder = [];
    @observable lastLoading = true;

    @observable doneOrder = [];
    @observable doneLoading = true;

    @observable OpenOrder = [];
    @observable OpenLoading = true;

    //#region getMarket

    @action async getMarket() {
        
        try{
            const { data } = await axios.get(`${API_BASE}/api/market/getmarket`,{
                headers:{
                    'Authorization': 'Bearer '+ AuthStore.token
                }
            })

            //console.log(data);
            runInAction(()=>{
                this.markets=data;
                this.loading=false;
            });

        }catch(e){
            console.log(e);
            this.loading=false;
        }
    }

    //#endregion

    //#region getOpenOrder

    @action async getOpenOrder(model) {
        
        console.log(JSON.stringify(model)+' Open Order');
        try{
            const { data } = await axios.post(`${API_BASE}/api/market/openorder`,JSON.stringify(model),{
                headers:{
                    'Authorization': 'Bearer '+ AuthStore.token,
                    'Content-Type':'application/json'
                }
            })

            //console.log(data);
            runInAction(()=>{
                this.OpenOrder=data;
                this.OpenLoading=false;
            });

        }catch(e){
            //console.log(e);
            this.OpenLoading=false;
        }
    }

    //#endregion

    //#region getDoneOrder

    @action async getDoneOrder(model) {
        console.log(JSON.stringify(model)+' Done Order');
        try{
            const { data } = await axios.post(`${API_BASE}/api/market/doneorder`,JSON.stringify(model),{
                headers:{
                    'Authorization': 'Bearer '+ AuthStore.token,
                    'Content-Type':'application/json'
                }
            })

            //console.log(data);
            runInAction(()=>{
                this.doneOrder=data;
                this.doneLoading=false;
            });

        }catch(e){
            //console.log(e);
            this.doneLoading=false;
        }
    }

    //#endregion

    //#region getLastOrder

    @action async getLastOrder(model) {
        console.log(JSON.stringify(model)+' LAST Order');
        try{

            const { data } = await axios.post(`${API_BASE}/api/market/lastorder`,JSON.stringify(model),
            {
                headers:{
                    'Authorization': 'Bearer '+ AuthStore.token,
                    'Content-Type':'application/json'
                }
            })

           // console.log(data);
            runInAction(()=>{
                this.lastOrder=data;
                this.lastLoading=false;
            });

        }catch(e){
            console.log(e);
            this.lastLoading=false;
        }
    }

    //#endregion


}

export default new MarketStore();