import {observable,action,runInAction} from 'mobx';
import axios from 'axios';
import {API_BASE} from '../constant';
import AuthStore from './AuthStore';

class TransactionStore {

    @observable walletList = [];
    @observable loading = true;

    @observable transactions = [];
    @observable t_loading = true;

    //#region getWallet

    @action async getWallet() {
        
        try{
            const { data } = await axios.get(`${API_BASE}/api/transaction/GetWallets`,{
                headers:{
                    'Authorization': 'Bearer '+ AuthStore.token
                }
            })

            console.log(data);
            runInAction(()=>{
                this.walletList=data;
                this.loading=false;
            });

        }catch(e){
            console.log(e);
            this.loading=false;
        }
    }

    //#endregion

    //#region getTransaction

    @action async getTransaction() {

        try{
            const { data } = await axios.get(`${API_BASE}/api/transaction/Get`,{
                headers:{
                    'Authorization': 'Bearer '+ AuthStore.token
                }
            })

            //console.log(data);
            runInAction(()=>{
                this.transactions=data;
                this.t_loading=false;
            });

        }catch(e){
            console.log(e);
            this.t_loading=false;
        }
    }

    //#endregion

}

export default new TransactionStore();