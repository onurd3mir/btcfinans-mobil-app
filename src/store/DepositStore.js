import {observable,action,runInAction} from 'mobx';
import axios from 'axios';
import {API_BASE} from '../constant';
import AuthStore from './AuthStore';

class DepositStore {

    @observable deposits = [];
    @observable loading = true;
    @observable banks = [];
    @observable bank_loading = true;

    //#region getDeposit

    @action async getDeposit() {
        
        try{
            const { data } = await axios.get(`${API_BASE}/api/deposit/get`,{
                headers:{
                    'Authorization': 'Bearer '+ AuthStore.token
                }
            })

            //console.log(data);

            runInAction(()=>{
                this.deposits=data;
                this.loading=false;
            });

        }catch(e){
            console.log(e);
            this.loading=false;
        }
    }

    //#endregion

    //#region getDepositBank

    @action async getDepositBank() {
       
        try{
            
            const response = await fetch(`${API_BASE}/api/deposit/depositBank`, {
                method: 'POST', // or 'PUT'
                //body: JSON.stringify(data), // data can be `string` or {object}!
                headers: {
                    'Authorization': 'Bearer '+ AuthStore.token
                }
              });

              const data = await response.json();
              let selectBanks=[];
              //console.log(data);

            runInAction(()=>{
                //this.banks=data;
                data.map((x) => {
                    let obj={ ['label']:x.db_bank_name, ['value']:x.db_id, ['id']:x.db_id };
                    selectBanks.push(obj);
                 })

                this.banks=selectBanks;
                this.bank_loading=false;
            });

        }catch(e){
            console.log(e);
            this.bank_loading=false;
        }
    }

    //#endregion

    //#region postDeposit

    @action async postDeposit(model) {

        try{
            
            const response = await fetch(`${API_BASE}/api/deposit/DepositPost`, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(model), // data can be `string` or {object}!
                headers: {
                    'Authorization': 'Bearer '+ AuthStore.token,
                    'Content-Type':'application/json'
                }
              });

            //   const data = await response.json();  
        }catch(e){
            console.log(e);
        }
    }

    //#endregion


}

export default new DepositStore();