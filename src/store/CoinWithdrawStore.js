import {observable,action,runInAction} from 'mobx';
import axios from 'axios';
import {API_BASE} from '../constant';
import AuthStore from './AuthStore';

class CoinWithdrawStore {

    @observable withdraws = [];
    @observable loading = true;

    //#region getWithdraw

    @action async getWithdraw() {
        
        try{
            const { data } = await axios.get(`${API_BASE}/api/coinwithdraw/Get`,{
                headers:{
                    'Authorization': 'Bearer '+ AuthStore.token
                }
            })

            console.log(data);
            runInAction(()=>{
                this.withdraws=data;
                this.loading=false;
            });

        }catch(e){
            console.log(e);
            this.loading=false;
        }
    }
}

export default new CoinWithdrawStore();