import {observable,action} from 'mobx';
import AsyncStorage from '@react-native-community/async-storage';
import NavigationService from '../NavigationService';

class AuthStore {

    @observable token = null;

    @action async saveToken(token){

        try{
            await AsyncStorage.setItem('token',token);
            this.setupAuth();
            const data = await AsyncStorage.getItem('token');
            console.log(data);
            //console.log(NavigationService);
        }
        catch(e){
            console.log(e);
        }

    }

    @action async removeToken(token){
        try{
            await AsyncStorage.removeItem('token',token);
            this.token=null;
            this.setupAuth();
        }
        catch(e){
            console.log(e);
        }

    }

    @action async setupAuth() {
        await this.getToken();
    }

    @action async getToken(){

        try{
            //AsyncStorage.removeItem('token');
            const token = await AsyncStorage.getItem('token');
            if(!token){
                NavigationService.navigate('Auth');
                return false;
            }

            this.token=token;
            NavigationService.navigate('App');
        }catch(e){

        }
    }

    




}

export default new AuthStore();
