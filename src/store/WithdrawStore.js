import {observable,action,runInAction} from 'mobx';
import axios from 'axios';
import {API_BASE} from '../constant';
import AuthStore from './AuthStore';

class WithdrawStore {

    @observable withdraws = [];
    @observable loading = true;

    @observable banks_acounts = [];
    @observable bank_loading = true;

    //#region getWithdraw

    @action async getWithdraw() {
        
        try{
            const { data } = await axios.get(`${API_BASE}/api/Withdraw/Get`,{
                headers:{
                    'Authorization': 'Bearer '+ AuthStore.token
                }
            })

            runInAction(()=>{
                this.withdraws=data;
                this.loading=false;
            });

        }catch(e){
            console.log(e);
            this.loading=false;
        }
    }

    //#endregion

    //#region getUserBank

    @action async getUserBank() {

        try{

            const response = await fetch(`${API_BASE}/api/user/UserBank`, {
                method: 'POST', // or 'PUT'
                //body: JSON.stringify(data), // data can be `string` or {object}!
                headers: {
                    'Authorization': 'Bearer '+ AuthStore.token
                }
              });

              const data = await response.json();
              let banks=[];
              //console.log(data);

            runInAction(()=>{

                data.map((x) => {
                    let obj={ ['label']:x.bank_name, ['value']:x.bank_id, ['id']:x.bank_id };
                    banks.push(obj);
                 })


                //this.banks_acounts=data;
                this.banks_acounts=banks;

                this.bank_loading=false;
            });

        }catch(e){
            console.log(e);
            this.bank_loading=false;
        }
    }

    //#endregion

    //#region postWidthdraw

    @action async postWidthdraw(model) {

        try{
            
            const response = await fetch(`${API_BASE}/api/Withdraw/WithdrawPost`, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(model), // data can be `string` or {object}!
                headers: {
                    'Authorization': 'Bearer '+ AuthStore.token,
                    'Content-Type':'application/json'
                }
              });

            //   const data = await response.json();  
        }catch(e){
            console.log(e);
        }
    }

    //#endregion


}

export default new WithdrawStore();