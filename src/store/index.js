import AuthStore from './AuthStore';
import DepositStore from './DepositStore';
import WithdrawStore from './WithdrawStore';
import TransactionStore from './TransactionStore';
import CoinWithdrawStore from './CoinWithdrawStore';
import MarketStore from './MarketStore';

export default {
    AuthStore,
    DepositStore,
    WithdrawStore,
    TransactionStore,
    CoinWithdrawStore,
    MarketStore
}