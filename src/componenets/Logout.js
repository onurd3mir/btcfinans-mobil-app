import React, { Component } from 'react';
import { Icon } from 'native-base';

import { inject } from 'mobx-react';

@inject('AuthStore')
export default class Logout extends Component {
  
  render() {
    return (
      <Icon  name="log-out" style={{color:'#FFFFFF'}} onPress={ () => this.props.AuthStore.removeToken() } />
    );
  }
}
