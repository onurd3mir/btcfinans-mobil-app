import React from 'react';
import {createAppContainer,createSwitchNavigator} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Icon, Body } from 'native-base';

import SignIn from './screens/SignIn';

const AuthStack = createStackNavigator({
    SignIn : {
        screen: SignIn
    }
},{
    headerMode:'none'
});


import Market from './screens/Market';
import Board from './screens/Board';
import Withdraw from './screens/Withdraw';
import Transaction from './screens/Transaction';


const ExchangeStack = createStackNavigator({
    Market: Market,
    Board: Board,
},{
    headerMode:'none'
});

const MoneyStack = createStackNavigator({
    Withdraw: Withdraw,
},{
    headerMode:'none'
});

const KriptoStack = createStackNavigator({
    Transaction: Transaction,
},{
    headerMode:'none'
});


const AppStack = createBottomTabNavigator({

    Market:{
        screen:ExchangeStack,
        navigationOptions:{
            title:'Market',
            tabBarIcon:({tintColor}) => <Icon type="FontAwesome"  name="exchange" style={{ color:tintColor }} />
        }
    },
    Money:{
        screen:MoneyStack,
        navigationOptions:{
            title:'Türk Lirası',
            tabBarIcon:({tintColor}) => <Icon type="FontAwesome" name="money" style={{ color:tintColor }} />
        }
    },
    Kripto:{
        screen:KriptoStack,
        navigationOptions:{
            title:'Kripto',
            tabBarIcon:({tintColor}) => <Icon type="FontAwesome" name="btc" style={{ color:tintColor }} />
        }
    }
},{
    initialRouteName:'Market',
    tabBarOptions:{
        activeTintColor:'#469d25',
        inactiveTintColor:'#bdc3c7',
        style:{
            backgroundColor:'#fdfefe'
        }
    }
})

//AuthLoading
import AuthLoading from './screens/AuthLoading';

const SwitchNavigator =  createSwitchNavigator(
    {
      AuthLoading: {
          screen:AuthLoading
      },
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  );


export default createAppContainer(SwitchNavigator);



