import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import { Row , Col,  } from 'native-base';

export default class Wallets extends Component {
  
_renderItem = ({item}) => {

    return(
        <View style={styles.item} >
        <Row>
            <Col>
                <Text>{item.wallet_coin} </Text>
                <Text >{item.wallet_address}</Text>
                { item.wallet_address_data!='0' && <Text>{item.wallet_address_data}</Text> }
            </Col>
        </Row>
        </View>
    )
}

  render() {

    const { list } = this.props
    
    return (
        <FlatList
            data={list}
            keyExtractor={item=>item.wallet_coin}
            renderItem={this._renderItem}
         />
    );
  }
}

const styles = StyleSheet.create({

    item:{
      flex:1,
      padding:10,
      marginTop:5,
      borderWidth:1,
      borderColor:'#EEE',
    },
  
  })