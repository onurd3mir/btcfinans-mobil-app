import React, { Component } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import {Row, Col, Text, Spinner, Item} from 'native-base';

import {observer,inject} from 'mobx-react';

@inject('TransactionStore')
@observer
export default class Yatirma extends Component {
    
    state = {
        refreshing:false,
    }

    componentDidMount = () => {
        this.load();
    };
  
    load = async () => {
  
        // this.props.TransactionStore.loading=true;
        // this.props.TransactionStore.getWallet();
    
        this.props.TransactionStore.t_loading=true;
        this.props.TransactionStore.getTransaction();
    
        this.setState({
            refreshing:false,
        })
      
    } 

    _renderItem = ({item}) => {

        return(
          <Item style={styles.item} >
            <Row>
              <Col>
                <Row>
                  <Col>
                  <Text style={{ color: item.status_name === 'Beklemede' ? '#e74c3c': '#469d25'}}>{item.status_name}</Text>
                  </Col>
                  <Col style={{alignContent:'flex-end',alignItems:'flex-end'}}>
                    <Text style={{fontSize:11}}>{item.create_date.substring(0,10)}</Text>
                  </Col>
                </Row>
                <Text  style={{fontSize:10}}>{item.amount.toFixed(8)} {item.coin_code} </Text>
                <Text style={{fontSize:9}} >{item.tx_id}</Text>
              </Col>
            </Row>
          </Item>
        )
      }
    
      renderFooter = () => {
    
        if (!this.props.TransactionStore.t_loading) return null;
        return(
            <View style={{paddingVertical:20}}>
             <Spinner size="small" color="#333" />
            </View>
        )
      }
    
      onRefresh = () => {
        this.setState({refreshing:true,},
          ()=>{this.load();})
      }
    

  render() {

    const { TransactionStore } = this.props

    return (
      
        <FlatList
        //ListHeaderComponent={<Wallets list={TransactionStore.walletList} />}
        ListFooterComponent={this.renderFooter}
        data={TransactionStore.transactions}
        keyExtractor={item=>item.transaction_id}
        renderItem={this._renderItem}
        refreshing={this.state.refreshing}
        onRefresh={this.onRefresh}
        /> 


    );
  }
}


const styles = StyleSheet.create({

    item:{
      flex:1,
      padding:10,
      borderBottomWidth:1,
      //marginHorizontal:5,
      //marginVertical:5,
      //borderRadius:10,
    },
  
  })