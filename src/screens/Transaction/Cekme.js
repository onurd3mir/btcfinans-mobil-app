import React, { Component } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import {Row, Col, Text, Spinner, Item} from 'native-base';

import {inject, observer } from 'mobx-react';

@inject('CoinWithdrawStore')
@observer
export default class Cekme extends Component {
    
    state = {
        refreshing:false,
    }
    
    componentDidMount = () => {
        this.load();
    };
    
    load = async () => {

        this.props.CoinWithdrawStore.loading=true;
        this.props.CoinWithdrawStore.getWithdraw();

        this.setState({
            refreshing:false,
        })
    
    }
    
    _renderItem = ({item}) => {
    
        return(
            <Item style={styles.item} >
            <Row>
                <Col>
                <Row>
                    <Col>
                        <Text style={{ color: item.status_name === 'Beklemede' ? '#e74c3c': '#469d25'}}>{item.status_name}</Text>
                    </Col>
                    <Col style={{alignContent:'flex-end',alignItems:'flex-end'}}>
                         <Text style={{fontSize:11}}>{item.cw_date.substring(0,10)}</Text>
                    </Col>
                </Row>
                <Text style={{fontSize:10}}>{item.cw_amount.toFixed(8)} {item.cw_coin} </Text>
                <Text style={{fontSize:9}} >{item.cw_tx}</Text>
                </Col>
            </Row>
            </Item>
        )
    }
    
    renderFooter = () => {

        if (!this.props.CoinWithdrawStore.loading) return null;

        return(
            <View style={{paddingVertical:20}}>
                <Spinner size="small" color="#333" />
            </View>
        )
    }
    
    onRefresh = () => {
        this.setState({refreshing:true,},
        ()=>{this.load();})
    } 


    render() {

        const { CoinWithdrawStore } = this.props

        return (
            <FlatList
                ListFooterComponent={this.renderFooter}
                data={CoinWithdrawStore.withdraws}
                keyExtractor={item=>item.cw_id}
                renderItem={this._renderItem}
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
            />

        );
    }
}

const styles = StyleSheet.create({

    item:{
      paddingHorizontal:5,
      paddingVertical:10,
    }
  
  })