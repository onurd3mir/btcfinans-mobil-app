import React, { Component } from 'react';
import { StatusBar, StyleSheet, } from 'react-native';
import { Root, Header, Left, Right, Tabs, Tab, Title } from 'native-base';

import Logout from '../../componenets/Logout';

import Yatirma from './Yatirma';
import Cekme from './Cekme';

export default class index extends Component {
  
  state = {
    activeTab:1,
  }
 
  render() {

    return (
      <Root>
        <StatusBar hidden={true} />

        <Header style={styles.header}>
         <Left style={{flex:1}} >
            <Title>BTCFİNANS</Title>
         </Left>
          <Right style={{flex:1}}>
            <Logout />
          </Right>
        </Header>

        <Tabs 
            tabBarUnderlineStyle={{ backgroundColor: this.state.activeTab === 1 ? '#469d25' : '#FF0000' }} 
          >
            <Tab 
            heading="YATIRMA" 
            tabStyle={{backgroundColor:'#FFFFFF'}} 
            activeTabStyle={{backgroundColor:'#fefefe'}} 
            textStyle={{color: '#212121'}} 
            activeTextStyle={{color: '#212121'}}
          
            >

             <Yatirma /> 
             
            </Tab>

            <Tab 
            heading="ÇEKME" 
            tabStyle={{backgroundColor:'#FFFFFF'}} 
            activeTabStyle={{backgroundColor:'#fefefe'}} 
            textStyle={{color: '#212121'}} 
            activeTextStyle={{color: '#212121'}}
            >
              <Cekme />
              
            </Tab> 
          </Tabs>

      </Root>
    );
  }
}

const styles = StyleSheet.create({

  header:{
    
    backgroundColor:'#469d25',
  },
  headerTitle:{
    textAlign:'center',
  },
  item:{
    flex:1,
    padding:10,
    borderBottomWidth:1,
    //marginHorizontal:5,
    //marginVertical:5,
    //borderRadius:10,
  },

})