import React, { Component } from 'react';
import { StyleSheet, FlatList, StatusBar, View } from 'react-native';
import { Header, Body, Container, Text, Title, Item, Left, Right, Content, Icon, Tab,Tabs , Root, Row, Col  }  from 'native-base';

import Logout from '../../componenets/Logout';

import BuyForm from './buyForm';
import SellForm from './SellForm';
import LastOrder from './LastOrder';
import DoneOrder from './DoneOrder';
import OpenOrder from './OpenOrder';

import io from 'socket.io-client';


export default class index extends Component {
  
state = {
  activeTab:1,
  buyData:[],
  sellData:[],
  from_coin:'',
  to_coin:''
}

componentDidMount = () => {

    const { getParam } = this.props.navigation;
    const from = getParam('from')
    const to = getParam('to')

    this.setState({from_coin:from,to_coin:to});

    this.io = io.connect('',{
      timeout:1000
    });

    const model = {
      from:from,
      to:to
    };  

     this.io.emit('Refreh',model);

     this.io.on('refresh_data',  data => {
       this.setState({buyData:data});
       //console.log(data);
     })

     this.io.on('refresh_data_sell',  data => {
      this.setState({sellData:data});
     // console.log(data);
    })

    this.io.on('DoneOrder',  data => {

       if(data.market_from==from && data.market_to==to) 
       {
          if(data.market_type=="buy")
          {
              const { buyData } = this.state
              let new_amount = 0;
              let index = buyData.findIndex(c => c.market_price == data.market_price);
              if (index > -1) {

                  //buyData[index].kalanmiktar += data.market_amount * 1;

                  if(data.process_type==1)
                  { 
                     new_amount = buyData[index].kalanmiktar + data.market_amount * 1;   
                     buyData[index].kalanmiktar += data.market_amount * 1;
                     this.setState({buyData})
                     //console.log(data.process_type);
                  }
                  else if(data.process_type==2)
                  {
                     new_amount = buyData[index].kalanmiktar - data.market_amount * 1;
                     buyData[index].kalanmiktar -= data.market_amount * 1;
                     this.setState({buyData})
                     //console.log(data.process_type);
                  }
                  else if(data.process_type==3)
                  {
                      new_amount = buyData[index].kalanmiktar - data.market_amount * 1;
                      buyData[index].kalanmiktar -= data.market_amount * 1;
                      this.setState({buyData})
                      //console.log(data.process_type);
                  }

                  if(new_amount<=0)
                  { 
                       console.log('girdi buy'); 
                       let price = buyData.filter(c=>c.market_price!=data.market_price);
                       this.setState({buyData:price})
                       //console.log(data.process_type);
                  }

              }
              else {
                  let index = buyData.findIndex(c => c.market_price < data.market_price);
                  if (index > -1) 
                  {
                    //console.log(data.market_amount);

                      buyData.splice(index, 0, {
                          market_price:data.market_price,
                          kalanmiktar:data.market_amount,
                          songuncelleme:'',
                          buy_ondalik:8,
                          sell_ondalik:2,
                      });

                      this.setState({buyData})
                  }
                  else 
                  {
                   // console.log('ekle')
                      buyData.push({
                          market_price:data.market_price,
                          kalanmiktar:data.market_amount,
                          songuncelleme:'',
                          buy_ondalik:8,
                          sell_ondalik:2,
                      });
                  }
                  this.setState({buyData})
              }
              
              //console.log(new_amount+' buy');  

              
          }
          else
          {
              const { sellData } = this.state
              let new_amount = 0;
              let index = sellData.findIndex(c => c.market_price == data.market_price);
              if (index > -1) 
              {
                  //sellData[index].kalanmiktar += data.market_amount * 1;

                  if(data.process_type==1)
                  { 
                     new_amount = sellData[index].kalanmiktar + data.market_amount * 1;   
                     sellData[index].kalanmiktar += data.market_amount * 1;
                     //console.log(data.process_type);
                     this.setState({sellData})
                  }
                  else if(data.process_type==2)
                  {
                     new_amount = sellData[index].kalanmiktar - data.market_amount * 1;
                     sellData[index].kalanmiktar -= data.market_amount * 1;
                     ///console.log(data.process_type);
                     this.setState({sellData})
                  }
                  else if(data.process_type==3)
                  {
                      new_amount = sellData[index].kalanmiktar - data.market_amount * 1;
                      sellData[index].kalanmiktar -= data.market_amount * 1;
                      //console.log(data.process_type);
                      this.setState({sellData})
                  }

                  if(new_amount<=0)
                  { 
                    console.log('girdi sell');
                    const price = sellData.filter(c=>c.market_price!=data.market_price);
                    this.setState({sellData:price})  
                    //console.log(price);

                  }

              }
              else 
              {
                  let index = sellData.findIndex(c => c.market_price > data.market_price);
                  if (index > -1) 
                  {
                    //console.log(data.market_amount);

                      sellData.splice(index, 0, {
                          market_price:data.market_price,
                          kalanmiktar:data.market_amount,
                          songuncelleme:'',
                          buy_ondalik:8,
                          sell_ondalik:2,
                      });

                      this.setState({sellData})
                  }
                  else 
                  {
                    //console.log('sell ekle')
                      sellData.push({
                          market_price:data.market_price,
                          kalanmiktar:data.market_amount,
                          songuncelleme:'',
                          buy_ondalik:8,
                          sell_ondalik:2,
                      });

                      this.setState({sellData})
                  }
              }
            
              //console.log(new_amount+' sell');  

            
          }

       } 

      //console.log(data);
    })

};

  buyDataItem = ({item}) => {

    return (
      <Item style={{height:30,borderRightWidth:1}} last>
        <Col style={{flex:1}}>
            <Text style={{fontSize:11,alignContent:'flex-start'}}>{item.kalanmiktar.toFixed(8)}</Text>
        </Col>
        <Col style={{flex:1,alignItems:'flex-end',right:2 }}>
            <Text style={{fontSize:11,color:'green'}}>{item.market_price.toFixed(this.ondalik(this.state.to_coin))}</Text>
        </Col>
      </Item>
    )
  }

  sellDataItem = ({item}) => {

    return (
      <Item  style={{height:30,borderLeftWidth:1}} last >

        <Col style={{flex:1,left:2}}>
            <Text style={{fontSize:11,color:'red'}}>{item.market_price.toFixed(this.ondalik(this.state.to_coin))}</Text>
        </Col>

        <Col style={{flex:1,alignItems:'flex-end',right:2 }}>
            <Text style={{fontSize:11,alignContent:'flex-start'}}>{item.kalanmiktar.toFixed(8)}</Text>
        </Col>
       
      </Item>
    )
  }

  buyDataHead = () => {

    return (
      <Item style={{height:30,borderRightWidth:1}} last>
        <Col style={{flex:1}}>
            <Text style={{fontSize:11,color:'green'}}>MİKTAR</Text>
        </Col>
        <Col style={{flex:1,alignItems:'flex-end',right:2 }}>
            <Text style={{fontSize:11,color:'green'}}>FİYAT</Text>
        </Col>
      </Item>
    )
  }

  sellDataHead = () => {

    return (
      <Item style={{height:30,borderLeftWidth:1}} last>

        <Col style={{flex:1,left:2 }}>
            <Text style={{fontSize:11,color:'red'}}>FİYAT</Text>
        </Col>
        <Col style={{flex:1,alignItems:'flex-end',right:2}}>
            <Text style={{fontSize:11,color:'red'}}>MİKTAR</Text>
        </Col>
        
      </Item>
    )
  }


  ondalik = (code) => {

    if (code == "TRY") { return 2; }
    else if (code == "BTC") { return 2; }
    else if (code == "ETH") { return 2; }
    else if (code == "LTC") { return 2; }
    else if (code == "DOGE") { return 5; }
    else if (code == "BNB") { return 2; }
    else if (code == "BEEX") { return 4; }
    else if (code == "XLM") { return 4; }
    else if (code == "TUSD") { return 4; }
    else if (code == "APPC") { return 4; }
    else if (code == "HOT") { return 5; }
    else if (code == "BYB") { return 4; }
    else if (code == "TRX") { return 5; }
    else if (code == "XRP") { return 4; }
    else if (code == "BAT") { return 4; }
    else if (code == "ADA") { return 4; }
    else if (code == "HYDRO") { return 5; }
    else { return 2; }

  }


  render() {

    const {buyData,sellData} = this.state

    const { goBack,getParam } = this.props.navigation;
    const from = getParam('from')
    const to = getParam('to')
    return (

      <Root>

        <StatusBar hidden={true} /> 

        <Header style={styles.header}>
         <Left style={{flex:1}} >
           <Icon type="FontAwesome" name="angle-left" style={{color:'#FFFFFF'}} onPress={ () => goBack() } />
         </Left>
          <Body style={{flex:1}} >
            <Title style={styles.headerTitle}>{to}/{from}</Title>
          </Body>
          <Right style={{flex:1}}>
            <Logout />
          </Right>
        </Header>

        <View style={{flex:0.8}}>
          <Tabs 
            tabBarUnderlineStyle={{ backgroundColor: this.state.activeTab === 1 ? '#469d25' : '#FF0000' }} 
          >
            <Tab 
            heading="AL" 
            tabStyle={{backgroundColor:'#FFFFFF'}} 
            activeTabStyle={{backgroundColor:'#fefefe'}} 
            textStyle={{color: '#212121'}} 
            activeTextStyle={{color: '#212121'}}
          
            >
              <BuyForm from={from} to={to} />
            </Tab>
            <Tab 
            heading="SAT" 
            tabStyle={{backgroundColor:'#FFFFFF'}} 
            activeTabStyle={{backgroundColor:'#fefefe'}} 
            textStyle={{color: '#212121'}} 
            activeTextStyle={{color: '#212121'}}
            
            >
              <SellForm from={from} to={to} />
            </Tab> 
          </Tabs>
        </View>

        <View style={{flex:1,}}>
          <Tabs 
             tabBarUnderlineStyle={{ backgroundColor: '#469d25',  }} 
            >
              <Tab 
              heading="ALIŞ- SATIŞ" 
              tabStyle={{backgroundColor:'#FFFFFF',}} 
              activeTabStyle={{backgroundColor:'#fefefe'}} 
              textStyle={{color: '#212121', fontSize:8}} 
              activeTextStyle={{color: '#212121', fontSize:8}}
            
              >
              <Row>
                <Col >
                    <FlatList 
                      ListHeaderComponent={this.buyDataHead}
                      data={buyData}
                      renderItem={this.buyDataItem}
                      keyExtractor={(item,index) => index}
                      
                    />
                </Col>

                <Col >
                  <FlatList 
                      ListHeaderComponent={this.sellDataHead}
                      data={sellData}
                      renderItem={this.sellDataItem}
                      keyExtractor={(item,index) => index}
                      contentContainerStyle={{marginLeft:-2}}
                    />
                </Col>


              </Row>
                
              </Tab>

              <Tab 
              heading="AÇIK EMİRLERİM" 
              tabStyle={{backgroundColor:'#FFFFFF'}} 
              activeTabStyle={{backgroundColor:'#fefefe'}} 
              textStyle={{color: '#212121', fontSize:8}} 
              activeTextStyle={{color: '#212121', fontSize:8}}
              >
               <OpenOrder from={from} to={to} />
              </Tab> 

              <Tab 
              heading="GEÇMİŞİM" 
              tabStyle={{backgroundColor:'#FFFFFF'}} 
              activeTabStyle={{backgroundColor:'#fefefe'}} 
              textStyle={{color: '#212121', fontSize:8}} 
              activeTextStyle={{color: '#212121', fontSize:8}} 
              >
               <DoneOrder from={from} to={to} />
              </Tab> 

              <Tab 
              heading="SON İŞLEMLER" 
              tabStyle={{backgroundColor:'#FFFFFF'}} 
              activeTabStyle={{backgroundColor:'#fefefe'}} 
              textStyle={{color: '#212121', fontSize:8}} 
              activeTextStyle={{color: '#212121', fontSize:8}}
              >
               <LastOrder from={from} to={to} />
              </Tab> 


            </Tabs>
        </View>

      </Root>
     );
  }
}

const styles = StyleSheet.create({

  header:{
    
    backgroundColor:'#469d25',
  },
  headerTitle:{
    textAlign:'center',
  },
  item:{
    paddingHorizontal:5,
    paddingVertical:10,
  }

})