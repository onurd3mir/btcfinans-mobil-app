import React, { Component } from 'react';
import { FlatList,StyleSheet, View } from 'react-native';
import { Col, Item, Text, Row } from 'native-base';

import {inject,observer} from 'mobx-react';

@inject('MarketStore')
@observer
export default class DoneOrder extends Component {

    componentDidMount = () => {

        const model = {
            from : this.props.from,
            to : this.props.to,
        };

        this.props.MarketStore.getDoneOrder(model);
    };

    renderHeader = () => {

        return (

            <Row style={{backgroundColor:'#dedede',height:40,padding:5}}>
                <Col></Col>
                <Col style={{alignItems:'flex-end',justifyContent:'center'}}>
                    <Text style={{fontSize:12}}>Miktar/{this.props.to}</Text>
                </Col>
                <Col style={{alignItems:'flex-end',justifyContent:'center'}}>
                    <Text style={{fontSize:12}}>Fiyat/Toplam/TL</Text>
                </Col>
            </Row>
        )

    }

    rendeItem = ({item}) => {

        return(

        <Item>
            <Col>
                <Text style={[styles.text,{color: item.market_type==='buy' ? '#469d25':'#FF0000' }]}>{ item.market_type==='buy' ? 'Alış':'Satış' }</Text>
                <Text style={styles.text}>{item.market_date}</Text>
            </Col>

            <Col style={{ alignItems:'flex-end'}}>
                <Text style={styles.text}>{item.market_amount.toFixed(8)}</Text>
            </Col>

            <Col style={{padding:5, alignItems:'flex-end'}}>
                <Text style={styles.text}>{item.market_price.toFixed(2)}</Text>
                <Text style={styles.text}>{item.market_total.toFixed(2)}</Text>
            </Col>
        </Item>
        )

    }
    


  render() {

    const {MarketStore} = this.props

    return (
      <FlatList
        ListHeaderComponent={this.renderHeader}
        data={MarketStore.doneOrder}
        renderItem={this.rendeItem}
        keyExtractor={item=>item.market_id}
      />
    );
  }
}

const styles = StyleSheet.create({

    text:{
        fontSize:11
    }
})