import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import {Item, Radio, Content, Row, Col, 
      Text, Input, Button, Left, Right,
      Spinner, Toast} from 'native-base';

import {Formik} from 'formik';
import * as Yup from 'yup';
import axios from 'axios';

import {API_BASE} from '../../constant';

import io from 'socket.io-client';

import {inject,observer} from 'mobx-react';

@inject('AuthStore')
@observer
export default class SellForm extends Component {

  state = {
    radioChecked:0
  }

  onChangeRadio = (value) => {
    this.setState({radioChecked:value})
  }

  _handelSubmit = async ({price,amount},bag) => {

      try{
          //console.log(price+'-',amount+'-',this.state.radioChecked);

          if(price!="" && amount!="") {

            const model = {
              from:this.props.from,
              to:this.props.to,
              type:this.state.radioChecked,
              price:parseFloat(price),
              amount:parseFloat(amount),
              buysellType:'sell'
            }
            
            //console.log(model);

            const { data } = await axios.post(`${API_BASE}/api/market/SellMarket`,JSON.stringify(model),{
                headers:{
                    'Authorization': 'Bearer '+ this.props.AuthStore.token,
                    'Content-Type':'application/json'
                }
            });

            if(data.status){

              this.io = io.connect('http://78.111.98.168:9085',{
                timeout:1000
              });
              this.io.emit('Order');
              
              Toast.show({
                text: 'Emir Verildi!',
                //buttonText: 'Tamam',
                duration: 2000,
                type: "danger"
              })
            }
            else{
              Toast.show({
                text: 'İşlem Başarısız!',
                //buttonText: 'Tamam',
                duration: 2000,
                //type: ""
              })
            }  

            bag.setSubmitting(false);
            console.log(data);  
            
            //console.log(model);  

          }
          else {

            bag.setSubmitting(false);
            Toast.show({
              text: 'Fiyat veya Miktar Giriniz!',
              //buttonText: 'Tamam',
              duration: 3000,
              //type: "danger"
            })
          }
      } 
      catch(e){
        bag.setSubmitting(false);
        bag.setErrors(e)
        alert(e);
      }

  }
 
  render() {
    return (
      <Formik
        initialValues={{
            amount:'',
            price:'',
        }}
        onSubmit={this._handelSubmit}
      >
       {({
         values,
         handleChange,
         handleSubmit,
         errors,
         touched,
         setFieldTouched,
         isValid,
         isSubmitting,

       }) => (
        <Content>

        <Row style={{marginTop:15}}>

          <Col style={{flex:1,flexDirection:'row',justifyContent:'center'}} onPress={()=> this.setState({radioChecked:0})}>
            <Text style={{marginRight:5}}>Limit</Text>
            <Radio
                color={"#d1d1d1"}
                selectedColor={"#FF0000"}
                selected={ this.state.radioChecked===0 ? true : false }
            />
          </Col>

          <Col style={{flex:1,flexDirection:'row', justifyContent:'center' }} onPress={()=>this.setState({radioChecked:2})}> 
            <Text style={{marginRight:5}}>Piyasa</Text>
            <Radio
                color={"#d1d1d1"}
                selectedColor={"#FF0000"}
                selected={this.state.radioChecked===2 ? true : false }      
            />
          </Col>

        </Row>

        <Item style={{width:'90%', marginLeft:'auto', marginRight:'auto', }}>

            <Input 
            onSubmitEditing={()=>this.amountRef._root.focus()}
            returnKeyType={'next'}
            onChangeText={handleChange('price')}
						value={values.price}
            placeholder="Fiyat"
            keyboardType="decimal-pad"
            onBlur={() => setFieldTouched('email')}
            autoCapitalize={'none'}
            autoCorrect={false}
            
             />
        </Item>

        <Item style={{width:'90%', marginLeft:'auto', marginRight:'auto', }}>
            <Input 
            ref={ref=>this.amountRef=ref}
            returnKeyType={'go'}
            onChangeText={handleChange('amount')}
						value={values.amount}
            placeholder="Miktar"
            keyboardType="decimal-pad"
            onBlur={() => setFieldTouched('email')}
            autoCapitalize={'none'}
            autoCorrect={false}
            />
        </Item>

        <Row style={{width:'90%', marginLeft:'auto', marginRight:'auto', marginTop:10, }}>
            <Left>
              <Button  
                block
                style={{backgroundColor:'#FF0000'}}
                disabled={!isValid || isSubmitting}
                onPress={handleSubmit}
              >
                { isSubmitting && <Spinner size={'small'} color={'white'} /> }
                <Text> SAT </Text>
              </Button>
            </Left>
            <Right>
              <Text>0.00</Text>
            </Right>
        </Row>

        </Content>
       )}  


      </Formik>
    );
  }
}

const styles = StyleSheet.create({

})