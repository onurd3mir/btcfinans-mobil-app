import React, { Component } from 'react';
import { View, Text } from 'react-native';

import {inject,observer} from 'mobx-react';

@inject('AuthStore')
@observer
export default class AuthLoading extends Component {
 
    componentDidMount = async () => {
        //this.props.AuthStore.removeToken(this.props.AuthStore.token);
        this.props.AuthStore.setupAuth();
    };
    
  render() {
    return (
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <Text> Loading.... </Text>
      </View>
    );
  }
}
