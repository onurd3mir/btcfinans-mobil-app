import React, { Component } from 'react';
import { StyleSheet, FlatList, StatusBar } from 'react-native';
import { Header, Body, Container, Text, Title, Item, Left, Right, Content, Icon }  from 'native-base';

import Logout from '../../componenets/Logout';

import {observer,inject} from 'mobx-react';

@inject('MarketStore')
@observer
export default class Market extends Component {
 
  componentDidMount = () => {
    this.props.MarketStore.getMarket();
  };

  _renderItem = ({item}) => {

    return (
    <Item last style={styles.item} onPress={()=>{ this.props.navigation.navigate('Board',{ from:item.market, to:item.coin }) }}>
      <Left>
        <Text>{item.coin}</Text>
      </Left>
    
      <Right>
        <Text> {item.last_price.toFixed(item.step)} TL</Text>
      </Right>
    
    </Item>
    )
  }
  
  render() {

    const {MarketStore} = this.props

    return (
     <Container>
       <StatusBar hidden={true} />
       <Header style={styles.header}  >
        <Left style={{flex:1}} />
         <Body style={{flex:1}} >
           <Title style={styles.headerTitle}>MARKET</Title>
         </Body>
         <Right style={{flex:1}}>
           <Logout />
         </Right>
       </Header>
       
       <Content>
      
        <FlatList
          data={MarketStore.markets}
          renderItem={this._renderItem}
          keyExtractor={item => item.coin}
        />
       </Content>

     </Container>
    );
  }
}

const styles = StyleSheet.create({

  header:{
    
    backgroundColor:'#469d25',
  },
  headerTitle:{
    textAlign:'center',
  },
  item:{
    paddingHorizontal:5,
    paddingVertical:10,
  }

})