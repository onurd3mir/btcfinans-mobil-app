import React, { Component } from 'react';
import { View ,StyleSheet } from 'react-native';
import { Text, Button, Item, Input, Spinner, Picker, Icon } from 'native-base';
import RNPickerSelect from 'react-native-picker-select';


import {Formik} from 'formik';
import * as Yup from "yup";


export default class Form extends Component {

    state = {
        selected2: undefined,
    }
  render() {

    const banks = this.props.banks

    const placeholder = {
        label: 'Banka Seçiniz',
        value: '',
    };

    return (
        
        <Formik
            initialValues={{
                amount:'',
                banka_id:'',  
            }}
            onSubmit={this.props.onSubmit}
            onValueChange={this._onValueChange2}
            validationSchema={
                Yup.object().shape({
                    amount: Yup
                        .string()
                        .required('Boş Bırakmayınız!'),
                    banka_id:Yup
                        .string()
                        .required('Boş Bırakmayınız')    
                    
                })
            }
        >
        {({
            values,
            handleChange,
            handleSubmit,
            errors,
            touched,
            setFieldTouched,
            isValid,
            isSubmitting,
            setFieldValue,
            

        }) => (
                <View style={{marginHorizontal:5}} >

                    {/* <Picker
                        mode="dropdown"
                        iosIcon={<Icon name="arrow-down" />}
                        style={{ borderWidth:1,borderColor:'#eee' }}
                        placeholder="Banka Seçiniz"
                        placeholderIconColor="#007aff"
                        selectedValue={this.state.selected2}
                        onValueChange={(itemValue, itemIndex) => {
                            setFieldValue('banka_id', itemValue)
                            this.setState({selected2: itemValue})
                        }}
                    >
                        {
                            banks.map( x=> <Picker.Item label={x.bank_name} value={x.bank_id} key={x.bank_id} /> )
                        }
                        
                        
                    </Picker> */}

                    <RNPickerSelect
                        items={banks}
                        itemKey={banks.id}
                        placeholder={placeholder}
                        useNativeAndroidPickerStyle={false}
                        placeholderTextColor={'#212121'}
                        onValueChange={(itemValue, itemIndex) => {
                            setFieldValue('banka_id', itemValue)
                            this.setState({selected2: itemValue})
                        }}
                    />

                    <Item error={errors.amount && touched.amount}>
                        <Input  
                            returnKeyType={'go'}
                            onChangeText={handleChange('amount')}
                            value={values.amount}
                            placeholder='Miktar Giriniz'
                            onBlur={() => setFieldTouched('amount')}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            keyboardType={'numeric'}/>

                        { (errors.amount && touched.amount) && <Text style={{color: 'red'}}>{errors.amount}</Text>}   
                    </Item>

                    <Button 
                        block success
                        disabled={!isValid || isSubmitting}
                        onPress={handleSubmit}
                        style={{marginTop: 10}}    >
                    { isSubmitting && <Spinner size={'small'} color={'white'} /> }
                        <Text>ÇEK</Text>
                    </Button> 

                </View>

              )}

        </Formik>

    );
  }
}
