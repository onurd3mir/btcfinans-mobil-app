import React, { Component } from 'react';
import { View, FlatList,StyleSheet } from 'react-native';
import {Row, Col, Text, Spinner, Item, Root} from 'native-base';


import DepositForm from './DepositForm';

import {observer,inject} from 'mobx-react';

@inject("DepositStore")
@observer
export default class Yatirma extends Component {

    state = {
        refreshing:false,
    }
  
    componentDidMount = () => {
      this.load();
    };
  
    load = async () => {
      
      this.props.DepositStore.loading=true;
      this.props.DepositStore.bank_loading=true;
  
      this.props.DepositStore.getDepositBank();
      this.props.DepositStore.getDeposit();
  
      this.setState({
        refreshing:false,
      })
  
    }
  
    _handleSubmit = async ({amount,banka_id}, bag) => {
      try { 
  
          let model= {
              deposit_amount:parseInt(amount),
              deposit_bank:banka_id
          };
          
          this.props.DepositStore.postDeposit(model);
          bag.setSubmitting(false);
          this.load();
  
      }
      catch(e) {
          console.warn(e);
          bag.setSubmitting(false);
      } 
    }
  
    onRefresh = () => {
        this.setState({
            refreshing:true,
        },()=>{
          this.load();
        })
    }
  
    renderDeposit = ({item}) => {
  
        return ( 
            <Item style={styles.depositItem}>
                <Row>
                  <Col style={{flex:1}}>
                    <Text style={{ color: item.status_name === 'Beklemede' ? '#e74c3c': '#469d25'}}>{item.status_name}</Text>
                    <Text style={styles.depositItemText}>{item.db_bank_name}</Text>
                    <Text style={styles.depositItemText}>{item.deposit_amount.toFixed(2)} TL</Text>
                  </Col>
                  <Col style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
                    <Text style={styles.depositDate}>{item.deposit_date.substring(0,10)}</Text>
                  </Col>
                </Row>
            </Item>    
        )
    }
  
    renderFooter = () => {
  
      if (!this.props.DepositStore.loading || !this.props.DepositStore.bank_loading) return null;
      return(
          <View style={{paddingVertical:20}}>
          <Spinner size="small" color="#333" />
          </View>
      )
    }
  

  render() {

    const { DepositStore } = this.props;

    return (
       <Root>

        <DepositForm banks={DepositStore.banks} onSubmit={this._handleSubmit} />  

        <FlatList
          ListFooterComponent={this.renderFooter}
          data={DepositStore.deposits}
          keyExtractor={item=>item.deposit_id}
          renderItem={this.renderDeposit}
          refreshing={this.state.refreshing}
          onRefresh={this.onRefresh}
        />

       </Root>
    );
  }
}

const styles = StyleSheet.create({

    depositItem:{
        paddingHorizontal:5,
        paddingVertical:10,
    },
    depositDate:{
      fontSize:10,
      color:'#454545'
    },
    depositItemText:{
      fontSize:10,
      color:'#454545'
    },
  });