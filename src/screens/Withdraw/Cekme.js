import React, { Component } from 'react';
import { View,FlatList, StyleSheet } from 'react-native';
import {Spinner, Row, Col, Text, Item, Root} from 'native-base';

import Form from './Form';

import {observer,inject} from 'mobx-react';

@inject('WithdrawStore')
@observer
export default class Cekme extends Component {
 
    state = {
        refreshing:false,
    }

    componentDidMount = () => {
          this.load();
    };
      
    load = async () => {
    
        this.props.WithdrawStore.loading=true;
        this.props.WithdrawStore.bank_loading=true;
    
        this.props.WithdrawStore.getUserBank();
        this.props.WithdrawStore.getWithdraw();
    
        this.setState({
          refreshing:false,
        })
        
    }
    
    _handleSubmit = async ({amount,banka_id}, bag) => {

        try { 
            let model= {
                withdraw_amount:parseInt(amount),
                withdraw_bank:banka_id
            };
            
            this.props.WithdrawStore.postWidthdraw(model);
            bag.setSubmitting(false);
    
            this.load();
    
        }
        catch(e) {
            console.warn(e);
            bag.setSubmitting(false);
        } 
    }
    
    _renderItem = ({item}) => {
    
        return(
          <Item style={[styles.Item]} >
            <Row>
              <Col style={{flex:1}}>
                <Text style={{ color: item.status_name === 'Beklemede' ? '#e74c3c': '#469d25'}}>{item.status_name}</Text>
                <Text style={{fontSize:11}} >{item.bank_name}</Text>
                <Text style={{fontSize:11}}>{item.withdraw_amount.toFixed(2)} TL</Text>
              </Col>
              <Col style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
                <Text style={{fontSize:10}}>{item.withdraw_date.substring(0,10)}</Text>
              </Col>
            </Row>
          </Item>
        )
    }
    
    renderFooter = () => {
    
        if (!this.props.WithdrawStore.loading || !this.props.WithdrawStore.bank_loading) return null;
        return(
            <View style={{paddingVertical:20}}>
             <Spinner size="small" color="#333" />
            </View>
        )
    }
    
    onRefresh = () => {
        this.setState({refreshing:true,},
          ()=>{this.load();})
    }


  render() {

    const { WithdrawStore } = this.props

    return (

        <Root>

            <Form banks={WithdrawStore.banks_acounts} onSubmit={this._handleSubmit} />

            <FlatList
                ListFooterComponent={this.renderFooter} 
                data={WithdrawStore.withdraws}
                keyExtractor={item=>item.Withdraw_id}
                renderItem={this._renderItem}
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
            /> 

        </Root>
      
        
        
    );
  }
}

const styles = StyleSheet.create({

    Item:{
        paddingHorizontal:5,
        paddingVertical:10,
    },
   
  });