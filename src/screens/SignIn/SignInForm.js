import React, { Component } from 'react';
import { StyleSheet, Image } from 'react-native';
import { Item, Input, Button, Text, Grid, Col, Row, Spinner } from 'native-base';

import {Formik} from 'formik';
import * as Yup from "yup";

import {API_BASE} from '../../constant';
import axios from 'axios';
import queryString from 'query-string';

import {observer,inject} from 'mobx-react';

@inject('AuthStore')
export default class SignInForm extends Component {

    _handleSubmit = async ({email,password}, bag) => {
		try {
            
             const  headers = {
                'Content-Type': 'application/x-www-form-urlencoded'
              };

                const fdata = {
                    grant_type:'password',
                    username:email,
                    password:password
                };

			const { data } = await axios.post(`${API_BASE}/token`, queryString.stringify(fdata),{
                headers:headers
            });

            bag.setSubmitting(false);
            //console.warn(data.access_token);
			this.props.AuthStore.saveToken(data.access_token);	
			
		}catch (e) {
			 bag.setSubmitting(false);
             bag.setErrors(e)
             alert(e);
		}
	};
  
  render() {

    return (
       <Formik
                initialValues={{
                    email:'',
                    password:''    
                }}
                onSubmit={this._handleSubmit}
                validationSchema={
                    Yup.object().shape({
                        email: Yup
                            .string()
                            .email()
                            .required(),
                         password :Yup
                            .string()
                            .min(6)
                            .required()
                    })
                }
                
       >
           {({
               values,
               handleChange,
               handleSubmit,
               errors,
               touched,
               setFieldTouched,
               isValid,
               isSubmitting,

           }) => (

            <Grid style={{flex: 1,justifyContent:'center'}}>

                <Image style={styles.img}  source={{uri:"https://www.btcfinans.com/Content/mail_logo.png"}} />

                <Row style={styles.Row}>
                    <Col>   
                        <Item error={errors.email && touched.email}>
                            <Input  
                                onSubmitEditing={()=>this.passwordRef._root.focus()}
                                returnKeyType={'next'}
								onChangeText={handleChange('email')}
								value={values.email}
								placeholder='E-mail'
								onBlur={() => setFieldTouched('email')}
                                autoCapitalize={'none'}
                                autoCorrect={false} />

                        { (errors.email && touched.email) && <Text style={{color: 'red'}}>{errors.email}</Text>}   
                        </Item>
                    </Col>
                </Row>

                <Row  style={styles.Row}>
                    <Col>
                        <Item error={errors.password && touched.password}>
                            <Input
                                returnKeyType={'go'}
                                ref={ref=>this.passwordRef=ref}
								onChangeText={handleChange('password')}
								value={values.password}
								placeholder='password'
								onBlur={() => setFieldTouched('password')}
								autoCapitalize={'none'}
								secureTextEntry={true} />

                            { (errors.password && touched.password) && <Text style={{color: 'red'}}>{errors.password}</Text>}
                        </Item>
                    </Col>
                </Row> 

                <Row  style={styles.Row}>
                    <Col>
                        <Button 
                            block success
                            disabled={!isValid || isSubmitting}
                            onPress={handleSubmit}
                            style={{marginTop: 10}}
                            >
                            { isSubmitting && <Spinner size={'small'} color={'white'} /> }
                            <Text>Giriş</Text>
                        </Button>  
                    </Col>
                </Row>   

            </Grid>
           


           )}
       </Formik>
    );
  }
}

const styles = StyleSheet.create({

    Row:{
        height:60
    },
    img:{
        width:350,height:100
    },


});
