import React, { Component } from 'react';
import { SafeAreaView, StyleSheet, StatusBar } from 'react-native';

import SignInForm from './SignInForm';

export default class SignIn extends Component {
  
  render() {
    return (
       <SafeAreaView style={styles.main}>
         <StatusBar hidden={true} />
        <SignInForm />
       </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

    main:{

        flex:1,
        justifyContent:'center',
        padding:10,
        backgroundColor:'#FFFFFF'

    } 
});
